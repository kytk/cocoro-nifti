#!/bin/bash

if [[ $# -lt 1 ]]; then
  echo "Please specify a name of sequence sets"
  echo "Usage: $0 <name of sequence set>"
  exit 1
fi

#coconif_02_prep2.sh $1
coconif_03_bids.sh $1
coconif_04_revert_id.sh $1
coconif_05_integrate.sh $1


