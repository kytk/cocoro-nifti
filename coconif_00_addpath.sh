#!/bin/bash
#A script to add this direcotry to ~/.bash_aliases or ~/.bash_profile

echo "Which OS are you using? Select number."
select os in "Linux" "MacOS" "quit"
do
  if [ "$os" = "quit" ] ; then
    echo "quit."
    exit 0
  fi

  if [ -z "$os" ] ; then
    continue
  elif [ $os == "Linux" ] ; then
    grep '# PATH for cocoro-nifti' ~/.bash_aliases > /dev/null
    if [ $? -eq 1 ]; then
      echo '' >> ~/.bash_aliases
      echo '# PATH for cocoro-nifti' >> ~/.bash_aliases
      echo "export PATH=\$PATH:$PWD" >> ~/.bash_aliases
      echo "PATH for scripts was added to ~/.bash_aliases"
      echo "Please close and re-open the terminal."
    fi
    break

  elif [ $os == "MacOS" ] ; then
    grep '# PATH for cocoro-nifti' ~/.bash_profile > /dev/null
    if [ $? -eq 1 ]; then
      echo '' >> ~/.bash_profile
      echo '# PATH for cocoro-nifti' >> ~/.bash_profile
      echo "export PATH=\$PATH:$PWD" >> ~/.bash_profile
      echo "PATH for scripts was added to ~/.bash_profile"
      echo "Please close and re-open the terminal."
    fi
    break
  
  fi
done
