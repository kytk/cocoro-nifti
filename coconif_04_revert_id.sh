#/bin/bash
# Revert ID from BIDS to COCORO and generate COCORO-Nifti
# Usage: coconif_04_revert_id.sh <name of sequence set>
# 28 July 2023 K.Nemoto

# For debugging
# set -x

orig_id_list=$(mktemp)
bids_id_list=$(mktemp)
pair_list=$(mktemp)

if [[ $# -lt 1 ]]; then
  echo "ERROR: Please specify the name of sequence set!"
  echo "Usage: $0 <name of sequence set>"
  exit 1
fi

setname=${1%/}
cd $setname
[[ -d COCORO-Nifti ]] || mkdir COCORO-Nifti

# extract ID from subjlist
cat subjlist_${setname}.txt | awk '{ print $2 }' > ${orig_id_list}
cat subjlist_${setname}.txt | awk '{ print $2 }' |\
    sed -e 's/_//g' -e 's/-//g' > ${bids_id_list}
paste ${bids_id_list} ${orig_id_list}  > ${pair_list}

# change BIDS-ID to original-ID for each subject
cat ${pair_list} | while read bidsid origid
do
  # copy Nifti/bidsid to COCORO-Nifti/origid (rename parent directory)
  cp -rv Nifti/sub-${bidsid} COCORO-Nifti/${origid}
  # rename every bidsid files with origid files 
  find COCORO-Nifti/${origid} -type f -name 'sub-*' | while read line
  do
    newline=$(echo $line | sed "s/sub-${bidsid}/${origid}/")
    mv $line $newline
  done
done

# Delete *.tsv
find . -name '*.tsv' -exec rm {} \;

echo "COCORO-Nifti files were generated successfully."
echo "Please check COCORO-Nifti directory."

exit

