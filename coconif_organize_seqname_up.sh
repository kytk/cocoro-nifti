#!/bin/bash

# script to organize DICOM directories under COCORO sequence set name
# K. Nemoto and J. Matsumoto 3 Aug 2023

for dir in $(ls -F | grep / | cut -c 1-11 | uniq)
do
  [[ -d ../$dir ]] || mkdir ../$dir
  mv ${dir}_* ../$dir
done

