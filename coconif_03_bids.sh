#!/bin/bash
# script to convert DICOM to BIDS
# Usage: coconif_03_bids.sh <name of sequence set>
# You need to prepare heuristic.py and subjlist.txt beforehand
# Prerequisites: install dcm2niix and heudiconv
# K. Nemoto 28 Jul 2023

# For debugging
#set -x

if [[ $# -lt 1 ]]; then
  echo "Please specify a name of sequence sets"
  echo "Usage: $0 <name of sequence set>"
  exit 1
fi

setname=${1%/}
heuristic=code/heuristic_${setname}.py
subjlist=subjlist_${setname}.txt

# cd to $setname
cd $setname

# delete previous .heudiconv
[[ -d Nifti/.heudiconv ]] && rm -rf Nifti/.heudiconv 

# Run heudiconv
# remove blank line beforehand using sed '/^$/d'
cat ${subjlist} | sed '/^$/d' | while read dname subj
do
  heudiconv -d DICOM/${dname}/*/* \
	    -o Nifti -f ${heuristic} -s ${subj}  \
	    -g all -c dcm2niix -b --overwrite 
done

# change permission
find Nifti -type d -exec chmod 755 {} \;
find Nifti -type f -exec chmod 644 {} \;

echo "Finish converting DICOM to BIDS"

exit

