#/bin/bash
# Collect COCORO-NIFTI files and save in the same directory
# Usage: coconif_05_integrate.sh <name of sequence set>
# 28 July 2023 K.Nemoto

# For debugging
# set -x

if [[ $# -lt 1 ]]; then
  echo "ERROR: Please specify the name of sequence set!"
  echo "Usage: $0 <name of sequence set>"
  exit 1
fi


setname=${1%/}
[[ -d COCORO-Nifti-all/${setname} ]] || mkdir -p COCORO-Nifti-all/${setname}

find ${setname}/COCORO-Nifti/ -mindepth 1 -maxdepth 1 -type d -exec cp -r {} COCORO-Nifti-all/${setname}/ \;

echo "COCORO-Nifti files are saved in COCORO-Nifti-all/${setname}."

exit

