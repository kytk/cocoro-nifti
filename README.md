# cocoro-nifti

- COCOROプロジェクトで収集しているDICOMファイルをBIDS形式に変換すると同時に、COCOROIDのNIFTIファイルも生成するプログラム集です

## 必要なプログラム

- dcm2niix, heudiconv, gdcm を事前にセットアップしてください
- heudiconv, gdcm は以下でインストールできます

    ```
    pip3 install heudiconv gdcm
    ```

## プログラムの準備

- リポジトリのクローン
    ```
    cd ~/git
    git clone https://gitlab.com/kytk/cocoro-nifti.git
    ```

- パスの設定
    ```
    cd cocoro-nifti
    ./coconif_00_addpath.sh
    ```

    - この後、ターミナルを再起動します

- ワーキングディレクトリの準備
    - 作業するディレクトリを決めてください
    - 以下のコマンドは、すべてこのワーキングディレクトリからタイプします
    - わざわざシークエンスセットに移動する必要はありません（移動するとエラーになります）

## プログラムの使い方
- 実際はコマンドを2回うつだけです

### 解析ディレクトリの生成
- ワーキングディレクトリの下に解析ディレクトリを準備します

    ```
    coconif_batch1.sh <シークエンスセット>
    ```

### ソートされたDICOMファイルのコピー
- シークエンスセット/DICOM の下にソートされたDICOMを置いてください

### BIDSへの変換およびCOCORO-Niftiの生成
- BIDS変換を行うと同時にCOCORO-Niftiを生成します

    ```
    coconif_batch2.sh <シークエンスセット>
    ```

- 以上でファイルが生成されます

