#!/bin/bash

if [[ $# -lt 1 ]]; then
  echo "Please specify a name of sequence sets"
  echo "Usage: $0 <name of sequence set>"
  exit 1
fi

coconif_01_prep1.sh $1

