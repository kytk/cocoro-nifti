#!/bin/bash
# preparation script for DICOM to BIDS conversion using heudiconv
# Part2. make a subject list

# K.Nemoto 28 Jul 2023

# For debugging
# set -x

if [[ $# -lt 1 ]]; then
  echo "Please specify a name of sequence sets"
  echo "Usage: $0 <name of sequence set>"
  exit 1
fi

# First argument is a name of sequence set (e.g. NCNP_MR5_S1)
setname=${1%/}

cd $setname

ls DICOM/ | sed 's/^/{subject}\t/' > subjlist_${setname}.txt
echo "subjlist for ${setname}  is prepared."

# replace white spaces with underscore
for i in $(seq 3)
do
  find DICOM -maxdepth $i -name '* *' | \
  while read line
  do newline=$(echo $line | sed -e 's/ /_/g' -e 's/__/_/g')
    echo $newline
    mv "$line" $newline
  done
done

echo "whitespaces are replaced with underscores"

exit
