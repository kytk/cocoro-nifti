#!/bin/bash
# preparation script for DICOM to NIFTI conversion using dcm2niix
# Part1. organize directory structure

# K.Nemoto 02 Aug 2023

# For debugging
# set -x

if [[ $# -lt 1 ]]; then
  echo "Please specify a name of sequence sets"
  echo "Usage: $0 <name of sequence set>"
  exit 1
fi

# First argument is a name of sequence set (e.g. NCNP_MR5_S1)
setname=$1

# Specify the path of batch_heudiconv.sh
batchpath=$(dirname $(command -v coconif_00_addpath.sh))

# prepare working directory
[[ -d $setname ]] || mkdir $setname

cd $setname

# prepare DICOM directory
[[ -d DICOM ]] || mkdir DICOM

# copy heurisctics stored in "code"
cp -r ${batchpath}/code .

# copy keywords stored in "keys"
cp -r ${batchpath}/keys .

echo "Directory for ${setname} is prepared."
echo "Please copy DICOM files under DICOM directory."

exit


