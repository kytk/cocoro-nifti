# heuristic.py for NCNP_MR5_S1 protocol
# 28 Jul 2023 K. Nemoto

import os, re


def create_key(template, outtype=('nii.gz',), annotation_classes=None):
    if template is None or not template:
        raise ValueError('Template must be a valid format string')
    return template, outtype, annotation_classes


def infotodict(seqinfo):
    """Heuristic evaluator for determining which runs belong where

    allowed template fields - follow python string module:

    item: index within category
    subject: participant id
    seqitem: run number during scanning
    subindex: sub index within group
    """

##### list keys for t1w, dwi, rs-fMRI below ###########################################################################

    t1w = create_key('sub-{subject}/anat/sub-{subject}_run-{item:02d}_T1w')
    t2w = create_key('sub-{subject}/anat/sub-{subject}_run-{item:02d}_T2w')
    func_rest_PA = create_key('sub-{subject}/func/sub-{subject}_dir-PA_task-rest_run-{item:02d}_bold')
    func_rest_AP = create_key('sub-{subject}/func/sub-{subject}_dir-AP_task-rest_run-{item:02d}_bold')
    dwi_dir099_PA = create_key('sub-{subject}/dwi/sub-{subject}_dir099-PA_run-{item:02d}_dwi')
    dwi_dir099_AP = create_key('sub-{subject}/dwi/sub-{subject}_dir099-AP_run-{item:02d}_dwi')
    dwi_dir107_PA = create_key('sub-{subject}/dwi/sub-{subject}_dir107-PA_run-{item:02d}_dwi')
    dwi_dir107_AP = create_key('sub-{subject}/dwi/sub-{subject}_dir107-AP_run-{item:02d}_dwi')
    dwi_dir099_AP_b0 = create_key('sub-{subject}/dwi/sub-{subject}_dir099-AP_b0_run-{item:02d}_dwi')
    dwi_dir107_AP_b0 = create_key('sub-{subject}/dwi/sub-{subject}_dir107-AP_b0_run-{item:02d}_dwi')
    fmap_PA =  create_key('sub-{subject}/fmap/sub-{subject}_dir-PA_run-{item:02d}_fieldmap')
    fmap_AP =  create_key('sub-{subject}/fmap/sub-{subject}_dir-AP_run-{item:02d}_fieldmap')
    info = {t1w:[], t2w:[], func_rest_PA:[], func_rest_AP:[], dwi_dir099_PA:[], dwi_dir099_AP:[], dwi_dir099_AP_b0:[], dwi_dir107_PA:[], dwi_dir107_AP:[], dwi_dir107_AP_b0:[], fmap_PA:[], fmap_AP:[]}

#######################################################################################################################

    for idx, s in enumerate(seqinfo):
        """
        The namedtuple `s` contains the following fields:

        * total_files_till_now
        * example_dcm_file
        * series_id
        * dcm_dir_name
        * unspecified2
        * unspecified3
        * dim1
        * dim2
        * dim3
        * dim4
        * TR
        * TE
        * protocol_name
        * is_motion_corrected
        * is_derived
        * patient_id
        * study_description
        * referring_physician_name
        * series_description
        * image_type
        """

##### extract keywords from sorted DICOM series-based sub-directories ##########

        if 'T1w_MPR' in s.dcm_dir_name:
            info[t1w].append(s.series_id)
        if 'T2w_SPC' in s.dcm_dir_name:
            info[t2w].append(s.series_id)
        if ('rfMRI_REST_PA' in s.dcm_dir_name) and (s.dim4 >= 200):    
            info[func_rest_PA].append(s.series_id)
        if ('rfMRI_REST_AP' in s.dcm_dir_name) and (s.dim4 >= 200):    
            info[func_rest_AP].append(s.series_id)
        if ('dMRI' in s.dcm_dir_name) and ('dir99' in s.dcm_dir_name) and ('PA' in s.dcm_dir_name) and (not 'SBRef' in s.dcm_dir_name) and (s.dim4 >= 30):    
            info[dwi_dir099_PA].append(s.series_id)
        if ('dMRI' in s.dcm_dir_name) and ('dir107' in s.dcm_dir_name) and ('PA' in s.dcm_dir_name) and (not 'SBRef' in s.dcm_dir_name) and (s.dim4 >= 30):    
            info[dwi_dir107_PA].append(s.series_id)
        if ('dMRI' in s.dcm_dir_name) and ('dir99' in s.dcm_dir_name) and ('AP' in s.dcm_dir_name) and (not 'SBRef' in s.dcm_dir_name) and (s.dim4 >= 30):    
            info[dwi_dir099_AP].append(s.series_id)
        if ('dMRI' in s.dcm_dir_name) and ('dir107' in s.dcm_dir_name) and ('AP' in s.dcm_dir_name) and (not 'SBRef' in s.dcm_dir_name) and (s.dim4 >= 30):    
            info[dwi_dir107_AP].append(s.series_id)
        if ('dMRI' in s.dcm_dir_name) and ('dir99' in s.dcm_dir_name) and ('AP' in s.dcm_dir_name) and (not 'SBRef' in s.dcm_dir_name) and (s.dim4 == 1):    
            info[dwi_dir099_AP_b0].append(s.series_id)
        if ('dMRI' in s.dcm_dir_name) and ('dir107' in s.dcm_dir_name) and ('AP' in s.dcm_dir_name) and (not 'SBRef' in s.dcm_dir_name) and (s.dim4 == 1):    
            info[dwi_dir107_AP_b0].append(s.series_id)
        if ('FieldMap' in s.dcm_dir_name) and ('PA' in s.dcm_dir_name):    
            info[fmap_PA].append(s.series_id)
        if ('FieldMap' in s.dcm_dir_name) and ('AP' in s.dcm_dir_name):    
            info[fmap_AP].append(s.series_id)

################################################################################
            
    return info
