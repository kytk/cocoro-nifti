# heuristic.py for KYTX_MR2_S1 protocol
# 28 Jul 2023 K. Nemoto

import os, re


def create_key(template, outtype=('nii.gz',), annotation_classes=None):
    if template is None or not template:
        raise ValueError('Template must be a valid format string')
    return template, outtype, annotation_classes


def infotodict(seqinfo):
    """Heuristic evaluator for determining which runs belong where

    allowed template fields - follow python string module:

    item: index within category
    subject: participant id
    seqitem: run number during scanning
    subindex: sub index within group
    """

##### list keys for t1w, dwi, rs-fMRI below ###########################################################################

    t1w = create_key('sub-{subject}/anat/sub-{subject}_run-{item:02d}_T1w')
    func_rest_PA = create_key('sub-{subject}/func/sub-{subject}_dir-PA_task-rest_run-{item:02d}_bold')
    dwi = create_key('sub-{subject}/dwi/sub-{subject}_run-{item:02d}_dwi')
    dwi_b0 = create_key('sub-{subject}/dwi/sub-{subject}_b0_run-{item:02d}_dwi')
    fmap_dwi_mag =  create_key('sub-{subject}/fmap/sub-{subject}_dwi_run-{item:02d}_magnitude')
    fmap_dwi_phase = create_key('sub-{subject}/fmap/sub-{subject}_dwi_run-{item:02d}_phasediff')
    fmap_rsf_mag =  create_key('sub-{subject}/fmap/sub-{subject}_rsf_run-{item:02d}_magnitude')
    fmap_rsf_phase = create_key('sub-{subject}/fmap/sub-{subject}_rsf_run-{item:02d}_phasediff')
    info = {t1w:[], func_rest_PA:[], dwi:[], dwi_b0:[], fmap_dwi_mag:[], fmap_dwi_phase:[], fmap_rsf_mag:[], fmap_rsf_phase:[]}

#######################################################################################################################

    for idx, s in enumerate(seqinfo):
        """
        The namedtuple `s` contains the following fields:

        * total_files_till_now
        * example_dcm_file
        * series_id
        * dcm_dir_name
        * unspecified2
        * unspecified3
        * dim1
        * dim2
        * dim3
        * dim4
        * TR
        * TE
        * protocol_name
        * is_motion_corrected
        * is_derived
        * patient_id
        * study_description
        * referring_physician_name
        * series_description
        * image_type
        """

##### extract keywords from sorted DICOM series-based sub-directories ##########

        if 'T1__MPRAGE' in s.dcm_dir_name:
            info[t1w].append(s.series_id)
        if ('bold_rs_jadni2_tr25_40slc_240vol_PA' in s.dcm_dir_name) and (s.dim4 >= 200):    
            info[func_rest_PA].append(s.series_id)
        if ('mb_diff_b1500_64dir_70slc_2mm_68' in s.dcm_dir_name) and (s.dim4 >= 30):    
            info[dwi].append(s.series_id)
        if ('mb_diff_b0_70slc_2mm' in s.dcm_dir_name) and (s.dim4 == 1):    
            info[dwi_b0].append(s.series_id)
        if 'field_mapping_jadni2' in s.dcm_dir_name and 'M' in s.image_type:    
            info[fmap_rsf_mag].append(s.series_id)
        if 'field_mapping_jadni2' in s.dcm_dir_name and 'P' in s.image_type:    
            info[fmap_rsf_phase].append(s.series_id)
        if 'field_mapping_magphase' in s.dcm_dir_name and 'M' in s.image_type:    
            info[fmap_dwi_mag].append(s.series_id)
        if 'field_mapping_magphase' in s.dcm_dir_name and 'P' in s.image_type:    
            info[fmap_dwi_phase].append(s.series_id)


################################################################################
            
    return info
