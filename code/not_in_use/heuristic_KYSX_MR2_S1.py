# heuristic.py for KYSX_MR2_S1 protocol
# 30 Jul 2023 K. Nemoto

import os, re


def create_key(template, outtype=('nii.gz',), annotation_classes=None):
    if template is None or not template:
        raise ValueError('Template must be a valid format string')
    return template, outtype, annotation_classes


def infotodict(seqinfo):
    """Heuristic evaluator for determining which runs belong where

    allowed template fields - follow python string module:

    item: index within category
    subject: participant id
    seqitem: run number during scanning
    subindex: sub index within group
    """

##### list keys for t1w, dwi, rs-fMRI below ###########################################################################

    t1w = create_key('sub-{subject}/anat/sub-{subject}_run-{item:02d}_T1w')

    info = {t1w:[]}

#######################################################################################################################

    for idx, s in enumerate(seqinfo):
        """
        The namedtuple `s` contains the following fields:

        * total_files_till_now
        * example_dcm_file
        * series_id
        * dcm_dir_name
        * unspecified2
        * unspecified3
        * dim1
        * dim2
        * dim3
        * dim4
        * TR
        * TE
        * protocol_name
        * is_motion_corrected
        * is_derived
        * patient_id
        * study_description
        * referring_physician_name
        * series_description
        * image_type
        """

##### extract keywords from sorted DICOM series-based sub-directories ##########

        if 'T1W_3D_TFE_1mm' in s.dcm_dir_name:
            info[t1w].append(s.series_id)

################################################################################
            
    return info
