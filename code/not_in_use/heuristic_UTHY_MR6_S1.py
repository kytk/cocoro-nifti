# heuristic.py for UTHY_MR6_S1
# 31 Jul 2023 K. Nemoto

import os

def create_key(template, outtype=('nii.gz',), annotation_classes=None):
    if template is None or not template:
        raise ValueError('Template must be a valid format string')
    return template, outtype, annotation_classes

def infotodict(seqinfo):
    """Heuristic evaluator for determining which runs belong where

    allowed template fields - follow python string module:

    item: index within category
    subject: participant id
    seqitem: run number during scanning
    subindex: sub index within group
    """

    ##### list keys for t1w, t2w, dwi, rs-fMRI, and filedmaps below ############

    # T1
    t1w = create_key('sub-{subject}/anat/sub-{subject}_run-{item:02d}_T1w')

    # Resting-state (only one phase encoding)
    func_rest = create_key('sub-{subject}/func/sub-{subject}_task-rest_run-{item:02d}_bold')

    # DWI (only one phase encoding)
    dwi = create_key('sub-{subject}/dwi/sub-{subject}_run-{item:02d}_dwi')


    # Field map
    fmap_mag =  create_key('sub-{subject}/fmap/sub-{subject}_magnitude')
    fmap_phase = create_key('sub-{subject}/fmap/sub-{subject}_phase')
    

    info = {t1w:[], func_rest:[], dwi:[], fmap_mag:[], fmap_phase:[]}

    ############################################################################

    for idx, s in enumerate(seqinfo):
        """
        The namedtuple `s` contains the following fields:

        * total_files_till_now
        * example_dcm_file
        * series_id
        * dcm_dir_name
        * unspecified2
        * unspecified3
        * dim1
        * dim2
        * dim3
        * dim4
        * TR
        * TE
        * protocol_name
        * is_motion_corrected
        * is_derived
        * patient_id
        * study_description
        * referring_physician_name
        * series_description
        * image_type
        """

        ### extract keywords from sorted DICOM series-based sub-directories and dimensions ###

        # T1w
        if 'MPRAGE' in s.dcm_dir_name:
            info[t1w].append(s.series_id)

        # rs-fMRI
        if 'rsfMRI' in s.dcm_dir_name:    
            info[func_rest].append(s.series_id)

        # DWI
        if 'DTI' in s.dcm_dir_name and not 'Reg' in s.dcm_dir_name:    
            info[dwi].append(s.series_id)

        # Fieldmap
        #if ('SPGR_TE4.9' in s.dcm_dir_name) and (s.[0x0043, 0x102f].value == 3):
        #    info[fmap_mag].append(s.series_id)
        #if ('SPGR_TE4.9' in s.dcm_dir_name) and (s.[0x0043, 0x102f].value == 2):
        #    info[fmap_phase].append(s.series_id)
        #if ('SPGR_TE7.3' in s.dcm_dir_name) and (s.[0x0043, 0x102f].value == 3):
        #    info[fmap_mag].append(s.series_id)
        #if ('SPGR_TE7.3' in s.dcm_dir_name) and (s.[0x0043, 0x102f].value == 2):
        #    info[fmap_phase].append(s.series_id)

            
    return info
