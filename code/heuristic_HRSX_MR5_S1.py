# heuristic.py for HRSX_MR5_S1 protocol
# 28 Jul 2023 K. Nemoto

import os, re


def create_key(template, outtype=('nii.gz',), annotation_classes=None):
    if template is None or not template:
        raise ValueError('Template must be a valid format string')
    return template, outtype, annotation_classes


def infotodict(seqinfo):
    """Heuristic evaluator for determining which runs belong where

    allowed template fields - follow python string module:

    item: index within category
    subject: participant id
    seqitem: run number during scanning
    subindex: sub index within group
    """

##### list keys for t1w, dwi, rs-fMRI below ###########################################################################

    t1w = create_key('sub-{subject}/anat/sub-{subject}_run-{item:02d}_T1w')
    func_rest = create_key('sub-{subject}/func/sub-{subject}_task-rest_run-{item:02d}_bold')
    dwi = create_key('sub-{subject}/dwi/sub-{subject}_run-{item:02d}_dwi')

    fmap_mag =  create_key('sub-{subject}/fmap/sub-{subject}_magnitude')
    fmap_phase = create_key('sub-{subject}/fmap/sub-{subject}_phasediff')

    info = {t1w: [], func_rest: [], dwi: [], fmap_mag: [], fmap_phase: []}

#######################################################################################################################

    for idx, s in enumerate(seqinfo):
        """
        The namedtuple `s` contains the following fields:

        * total_files_till_now
        * example_dcm_file
        * series_id
        * dcm_dir_name
        * unspecified2
        * unspecified3
        * dim1
        * dim2
        * dim3
        * dim4
        * TR
        * TE
        * protocol_name
        * is_motion_corrected
        * is_derived
        * patient_id
        * study_description
        * referring_physician_name
        * series_description
        * image_type
        """

##### extract keywords from sorted DICOM series-based sub-directories ##########

        if 'T1' in s.dcm_dir_name:
            info[t1w].append(s.series_id)
        if 'REST' in s.dcm_dir_name:    
            info[func_rest].append(s.series_id)
        if ('DWI' in s.dcm_dir_name) and (s.dim4 >= 30):    
            info[dwi].append(s.series_id)
        if 'fieldmap' in s.dcm_dir_name and 'M' in s.image_type:    
            info[fmap_mag].append(s.series_id)
        if 'fieldmap' in s.dcm_dir_name and 'P' in s.image_type:    
            info[fmap_phase].append(s.series_id)

################################################################################
            
    return info
