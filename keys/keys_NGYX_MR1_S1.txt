#vendor
vendor=Siemens

#key for t1w
t1w=t1_mpr_sag_p2_iso

#key for func
func=ep2d_bold_resting

#key for dwi
dwi=ep2d_diff_64dir

#key for fm1
fm1=''

#key for fm2
fm2=''
