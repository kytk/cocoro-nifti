#!/bin/bash
# script for DICOM to NIFTI conversion using dcm2niix

# K.Nemoto 02 Aug 2023

# For debugging
set -x

if [[ $# -lt 1 ]]; then
  echo "Please specify a name of sequence sets"
  echo "Usage: $0 <name of sequence set>"
  exit 1
fi

# First argument is a name of sequence set (e.g. NCNP_MR5_S1)
setname=${1%/}

cd "$setname"

# source configuration file
source keys/keys_"$setname".txt

ls DICOM/ > "subjlist_dcm2niix_${setname}.txt"
echo "subjlist for ${setname} is prepared."

# replace white spaces with underscores
for i in $(seq 3); do
  find DICOM -maxdepth "$i" -name '* *' | \
  while read -r line; do
    new_line=$(echo "$line" | sed -e 's/ /_/g' -e 's/__/_/g')
    echo "$new_line"
    mv "$line" "$new_line"
  done
done

echo "whitespaces are replaced with underscores"

cwd="$PWD"

# prepare directories for subjid
cat "subjlist_dcm2niix_${setname}.txt" | \
while read -r subjid; do
  [[ -n "$t1w" ]] && mkdir -p "COCORO-Nifti/${subjid}/anat"
  [[ -n "$dwi" ]] && mkdir -p "COCORO-Nifti/${subjid}/dwi"
  [[ -n "$fm1" ]] && mkdir -p "COCORO-Nifti/${subjid}/fmap"
  [[ -n "$func" ]] && mkdir -p "COCORO-Nifti/${subjid}/func"

  # set variables
  nii_wd="COCORO-Nifti/${subjid}"
  dcm_wd="DICOM/${subjid}"

  # T1w
  if [[ -n "$t1w" ]]; then
    dcm2niix -o "${nii_wd}/anat" -f "${subjid}_run-01_T1w" "${dcm_wd}"/*$t1w
    fslreorient2std "${nii_wd}/anat/${subjid}_run-01_T1w" "${nii_wd}/anat/${subjid}_run-01_T1w_std"
    rm "${nii_wd}/anat/${subjid}_run-01_T1w.nii"
    mv "${nii_wd}/anat/${subjid}_run-01_T1w_std.nii.gz" "${nii_wd}/anat/${subjid}_run-01_T1w.nii.gz"
    if [[ -f "${nii_wd}/anat/${subjid}_run-01_T1wa.nii" ]]; then
      fslreorient2std "${nii_wd}/anat/${subjid}_run-01_T1wa.nii" "${nii_wd}/anat/${subjid}_run-02_T1w.nii.gz"
      rm "${nii_wd}/anat/${subjid}_run-01_T1wa.nii"
      mv "${nii_wd}/anat/${subjid}_run-01_T1wa.json" "${nii_wd}/anat/${subjid}_run-02_T1w.json"
    fi
  fi

  # func
  if [[ -n "$func" ]]; then
    dcm2niix -z y -o "${nii_wd}/func" -f "${subjid}_task-rest_run-01_bold" "${dcm_wd}"/*$func
  fi

  # dwi
  if [[ -n "$dwi" ]]; then
    dcm2niix -z y -o "${nii_wd}/dwi" -f "${subjid}_run-01_dwi" "${dcm_wd}"/*$dwi
  fi

  # fmap
  if [[ -n "$fm1" ]] && [[ "$fm1" == "$fm2" ]]; then
    [[ -d "${dcm_wd}/fmtmp" ]] || mkdir "${dcm_wd}/fmtemp"
    find ${dcm_wd}/*${fm1} -type f -exec cp {} "${dcm_wd}/fmtemp" \;
    dcm2niix -z y -o "${nii_wd}/fmap" -f "${subjid}_run-01" "${dcm_wd}/fmtemp"
    rm -rf "${dcm_wd}/fmtemp"
    pushd "${nii_wd}/fmap"
    ls | rename 's/e2_ph/phasediff/'
    ls | rename 's/_e1/_magnitude1/'
    ls | rename 's/_e2/_magnitude2/'
    popd
  else
    if [[ -n "$fm1" ]]; then
      dcm2niix -z y -o "${nii_wd}/fmap" -f "${subjid}_run-01_magnitude1" "${dcm_wd}"/*$fm1
    fi
    if [[ -n "$fm2" ]]; then
      dcm2niix -z y -o "${nii_wd}/fmap" -f "${subjid}_run-01_magnitude2" "${dcm_wd}"/*$fm2
    fi
  fi

  if [[ -n "$fm1" ]]; then
    pushd "${nii_wd}/fmap"
    if [[ "$vendor" == 'GE' ]]; then
      find . -name '*real*' -exec rm {} \;
      find . -name '*imaginary*' -exec rm {} \;
      ls | rename 's/magnitude1_ph/phase1/' 
      ls | rename 's/magnitude2_ph/phase2/' 
    elif [[ "$vendor" == 'Philips' ]]; then
#      find . -name '*real*' -exec rm {} \;
#      find . -name '*imaginary*' -exec rm {} \;
      ls | rename 's/magnitude1_e1_ph/phase1/'
      ls | rename 's/magnitude1_e1/magnitude1/'
      ls | rename 's/magnitude1_e2_ph/phase2/'
      ls | rename 's/magnitude1_e2/magnitude2/'
    fi
    popd 
  fi
done

